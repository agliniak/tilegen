Gui = {}



--also will need to pass in input handler
function Gui:new(stateMachine)
	local newGui = {}
	setmetatable(newGui, self)
	self.__index = self

	--GUI can know about state machine, and input handle
	--gui then should update all updatabels, buttons etc, 
	--and retrieve the state, pressed un preseed etc, 
	love.mouse.setVisible(false)

	newGui.pstatemachine = stateMachine
	--temp gui load until gui to be loaded, lines, art buttons, cursors, etc are in a lua file
	newGui.buttons = {}
	table.insert(newGui.buttons,Button:new(850,50,150,50,newColor(60,60,60,255),"Spawn","SPAWN_POINT_MODE",stateMachine))
	table.insert(newGui.buttons,Button:new(850,125,150,50,newColor(60,60,60,255),"End Point","END_POINT_MODE",stateMachine))
	table.insert(newGui.buttons,Button:new(850,200,150,50,newColor(60,60,60,255),"Platform","PLATFORM_MODE",stateMachine))
	table.insert(newGui.buttons,Button:new(850,275,150,50,newColor(60,60,60,255),"Clear","CLEAR",stateMachine))


	newGui.cursor = Cursor:new()

	--should all rects, buttons etc, have labels? names? ids?
	newGui.rectangles = newGui:loadGuiFromFile()
	--table.insert(newGui.rectangles,Rectangle:new(25,25,800,600,newColor(255,255,255,255),"line"))
	--table.insert(newGui.rectangles,Rectangle:new(25,635,800,20,newColor(255,255,255,255),"line"))
	return newGui
end

function Gui:draw()
	--draw all elements
	

	--later, go thru all butons in table and draw

	--draw buttons
	for i,Rectangle in ipairs(self.buttons) do
		Rectangle:draw()
	end
	for i,Rectangle in ipairs(self.rectangles) do
		Rectangle:draw()
	end



	self.cursor:draw()

end

function Gui:update()
	self.cursor:update()

	for i,Button in ipairs(self.buttons) do
		Button:update()
	end

	--check bounds

	-- check mouse postion is inside button rect
	if love.mouse.getX() <= self.rectangles[1].x+self.rectangles[1].w and
   		love.mouse.getX() >= self.rectangles[1].x and
  		love.mouse.getY() <= self.rectangles[1].y+self.rectangles[1].h and
   		love.mouse.getY() >= self.rectangles[1].y then

   		
   		self.pstatemachine:AddState("EDIT_MODE")
   		self.pstatemachine:RemoveState("NONE_EDIT_MODE")
   		--print("EDITMODE")
  	return true
	else
		self.pstatemachine:AddState("NONE_EDIT_MODE")
		self.pstatemachine:RemoveState("EDIT_MODE")
		--print("NONE edit mode")
	return false
	end
end
	--



function Gui:loadGuiFromFile()

newGuiload = {}
	newGuiRects = {}

	for line in love.filesystem.lines("guiLoad.lua") do
		table.insert(newGuiload,line)
	end

	for i = 1, table.getn(newGuiload) do


		run = assert(loadstring("return "..newGuiload[i]))

		local testRect = run()

		table.insert(newGuiRects,testRect)
	end

	return newGuiRects
end