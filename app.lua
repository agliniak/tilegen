App = {}

function App:new()
	local newApp = {}
	setmetatable(newApp, self)
	self.__index = self

	return newApp
end

function App:load()
	math.randomseed(os.time())
	love.filesystem.setIdentity("tilegen")
	--make array of 1900 for 16x16 tiles on 800x600 with randomw 0-255
	-- screenTiles = {}
	-- tileIndex = 1
	-- for rowIndex = 0, 50-1 do
	-- 	for columnIndex = 0, 38-1 do 
	-- 		screenTiles[tileIndex] = math.random(0,255)
	-- 		tileIndex=tileIndex+1
	-- 	end
	-- end

	--try with a multidemsional array
	rowRes = 800
	colRes = 600



	screenTiles2 = {}
	
	for rowIndex = 1, rowRes do
		screenTiles2[rowIndex] = {}
		for columnIndex = 1, colRes do 
			
			screenTiles2[rowIndex][columnIndex] = math.random(0,2000)
		end

	end
	

	--print multidemsional display
	-- for rowIndex = 1, rowRes do
		
	-- 	for columnIndex = 1, colRes do 
			
	-- 		io.write(screenTiles2[rowIndex][columnIndex]..",")
	-- 	end
	-- 	print()
	-- end


numberOfAverages = 5

realdeepwater = 300
deepwater = 400
watercutoff=1025

sandcutoff = 1200
lightgrass = 1300
darkgrass = 1400
lowermount = 1600
highmount = 1800


dirt = 1400

for i =0, numberOfAverages do
	for rowIndex = 1, rowRes do
		
		for columnIndex = 1, colRes do 
			
			--io.write(screenTiles2[rowIndex][columnIndex]..",")
			local oldValue = screenTiles2[rowIndex][columnIndex]
			local average = 0
			local NumstoAvg = 0

			if      rowIndex == rowIndex and columnIndex + 1 <= colRes then 
						average= average + screenTiles2[rowIndex][columnIndex+1]
						--print(screenTiles2[rowIndex][columnIndex+1])
						NumstoAvg = NumstoAvg + 1
						--print("pass1")
						end

			if 	rowIndex-1 >= 1 and columnIndex + 1 <= colRes  then
						average = average + screenTiles2[rowIndex-1][columnIndex+1]
						NumstoAvg = NumstoAvg + 1
						--print(screenTiles2[rowIndex-1][columnIndex+1])
						--print("pass2")
						end

			if  rowIndex-1 >= 1 and columnIndex ==columnIndex  then
						average = average + screenTiles2[rowIndex-1][columnIndex]
						NumstoAvg = NumstoAvg + 1
						--print(screenTiles2[rowIndex-1][columnIndex])
						--print("pass3")
						end

			if  rowIndex-1 >= 1 and columnIndex - 1 >= 1  then
						average = average + screenTiles2[rowIndex-1][columnIndex-1]
						NumstoAvg = NumstoAvg + 1
						--print("pass4")
						end

			if  rowIndex == rowIndex and columnIndex -1 >= 1  then
						average = average + screenTiles2[rowIndex][columnIndex-1]
						NumstoAvg = NumstoAvg + 1
						--print("pass5")
						end

			if  rowIndex+1 <= rowRes and columnIndex-1 >= 1 then
						average = average + screenTiles2[rowIndex+1][columnIndex-1]
						NumstoAvg = NumstoAvg + 1
						--print("pass6")
						end

			if  rowIndex+1 <= rowRes and columnIndex ==columnIndex  then
						average = average + screenTiles2[rowIndex+1][columnIndex]
						NumstoAvg = NumstoAvg + 1
						--print("pass7")
						end

			if  rowIndex+1 <= rowRes and columnIndex + 1 <=colRes  then
						average = average + screenTiles2[rowIndex+1][columnIndex+1]
						NumstoAvg = NumstoAvg + 1
						--print("pass8")
			end

			average = average/NumstoAvg
			-- print("AVERAGE".. average.."Nums: "..NumstoAvg)


			screenTiles2[rowIndex][columnIndex] = average








		end
		
	end
	
	end


--print averaged array
	-- for rowIndex = 1, rowRes do
		
	-- 	for columnIndex = 1, colRes do 
			
	-- 		io.write(screenTiles2[rowIndex][columnIndex]..",")
	-- 	end
	-- 	print()
	-- end


--watercutoff makes them 0
	-- for rowIndex = 1, 50 do
		
	-- 	for columnIndex = 1, 38 do 
			
	-- 		if screenTiles2[rowIndex][columnIndex] <=watercutoff then
	-- 			screenTiles2[rowIndex][columnIndex] = 0
	-- 		end
	-- 	end
		
	-- end
	

	


	-- averagedTiles = {}
	-- --with each number, check and average all those around it.
	-- averageIndex = 1
	-- for rowIndex = 0, 50-1 do
	-- 	for columnIndex = 0, 38-1 do 




	-- 		--screenTiles[averageIndex] = average of numbers around it


	-- 		averageIndex = averageIndex+1

	-- 	end
	-- end




	







	
	-- self.testmap = self:loadMap()() -- loads then runs which returns the info in file into table

	-- tileset = love.graphics.newImage(self.testmap.tilesets[1].image)
	-- tilesetWidth = tileset:getWidth()
	-- tilesetHeight = tileset:getHeight()

	-- quads = {} 
	
	-- quadIndex = 1

	-- for rowIndex = 0, (tilesetHeight/self.testmap.tileheight)-1 do

	-- 	for columnIndex = 0, (tilesetWidth/self.testmap.tilewidth)-1 do

	-- 		quads[quadIndex] = love.graphics.newQuad(columnIndex*self.testmap.tilewidth,
	-- 												 rowIndex*self.testmap.tileheight,
	-- 												 self.testmap.tilewidth,self.testmap.tileheight,
	-- 												 tilesetWidth,tilesetHeight)
			
	-- 		quadIndex=quadIndex+1
	-- 	end
	-- end
end

function App:loadControls()
	
	return love.filesystem.load("controls.lua")
end



function App:draw()

	for rowIndex = 1, rowRes do
		
		for columnIndex = 1, colRes do 
			
			--io.write(screenTiles2[rowIndex][columnIndex]..",")
			local value = screenTiles2[rowIndex][columnIndex]
			
			
			if value < realdeepwater then
				love.graphics.setColor(0,0,70,255)
			love.graphics.point(rowIndex, columnIndex)
			end

			if value > realdeepwater and value <= deepwater then
				love.graphics.setColor(0,0,120,255)
			love.graphics.point(rowIndex, columnIndex)
			end

			if value > deepwater and value <= watercutoff then
				love.graphics.setColor(0,0,150,255)
			love.graphics.point(rowIndex, columnIndex)
			end
			if value > watercutoff and value <= sandcutoff then
				love.graphics.setColor(171,140,15,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > sandcutoff and value <= lightgrass then
				love.graphics.setColor(123,178,15,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > lightgrass and value <= darkgrass then
				love.graphics.setColor(10,90,30,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > darkgrass and value <= lowermount  then
				love.graphics.setColor(50,50,50,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			if value > lowermount   then
				love.graphics.setColor(150,150,150,255)
				love.graphics.point(rowIndex, columnIndex)
			end
			


			--love.graphics.setColor(value,value,value,value)
			--love.graphics.point(rowIndex, columnIndex)

		end
		
	end


	-- dataIndex = 1
	-- for rowIndex=1, self.testmap.height do --start at top, go down
	
	-- 	for columnIndex =1, self.testmap.width do
		
	-- 		love.graphics.draw(tileset,quads[self.testmap.layers[1].data[dataIndex]],(columnIndex-1)*self.testmap.tilewidth,(rowIndex-1)*self.testmap.tileheight)
	-- 		dataIndex=dataIndex+1
	-- 	end
	-- end
end

function App:update(dt)
	function love.keypressed(key,isrepeat)
		if key == "b" then

			local file = love.filesystem.newFile("map.lua")
					file:open("w")
					file:write("MAP(")
						print(love.filesystem.getAppdataDirectory())

					file:close()
				end
	end
	
end
