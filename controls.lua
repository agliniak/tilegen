local controls = {}

controls.up 	= "w"
controls.down 	= "s"
controls.left 	= "a"
controls.right  = "d"
controls.jump	= " "

return controls