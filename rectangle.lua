Rectangle = {}

function Rectangle:new(x,y,w,h,r,g,b,a,fill)
	local newRectangle = {}
	setmetatable(newRectangle, self)
	self.__index = self

	newRectangle.x = x
	newRectangle.y = y
	newRectangle.w = w
	newRectangle.h = h
	newRectangle.x2 = newRectangle.x+newRectangle.w
	newRectangle.y2 = newRectangle.y+newRectangle.h
	newRectangle.color = {}
	newRectangle.color.r = r
	newRectangle.color.g = g
	newRectangle.color.b = b
	newRectangle.color.a = a
	newRectangle.fill = fill
	newRectangle.minkowski=HitBox:new()

	
	newRectangle.xVel = 0
	newRectangle.yVel = 0

	return newRectangle
end

function Rectangle:getCenter()
	local center = {}
	center.x = (self.x+self.w)/2
	center.y = (self.y+self.w)/2
	return center
end

function Rectangle:setColor(r,g,b,a)
 	self.color.r=r
 	self.color.g=g
 	self.color.b=b
 	self.color.a=a
end

function Rectangle:draw()
	
	love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)
	love.graphics.rectangle(self.fill, self.x, self.y, self.w, self.h)
	self.minkowski:draw()
end
