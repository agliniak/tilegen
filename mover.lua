
--idea for making a component called movable 
Movable = {}

function Movable:new()  --x,y or nothing?
	local newMovable = {}
	setmetatable(newMovable, self)
	self.__index = self

	newMovable.location 	= Vector2d:new(0,0)
	newMovable.velocity 	= Vector2d:new(0,0)
	newMovable.acceleration = Vector2d:new(0,0)
	newMovable.topspeed 	= 2
	newMovable.mass 		= 10

	return newMovable
end
--load() load from file, the speed and mass.