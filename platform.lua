--Platform is a vector, keyboard movable, drawable, entity
Platform = {}

function Platform:new()
	local newPlatform = {}
	setmetatable(newPlatform, self)
	self.__index = self
	
	newPlatform.hitBox = HitBox:new()
	newPlatform.hitBox.visible = true
	return newPlatform

end

function Platform:draw()
	self.hitBox:draw()
	
end

function Platform:update()
	self.hitBox:draw()
	
end